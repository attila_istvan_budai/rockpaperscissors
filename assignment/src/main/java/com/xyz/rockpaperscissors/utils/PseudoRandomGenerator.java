package com.xyz.rockpaperscissors.utils;

public class PseudoRandomGenerator implements RandomGenerator{
	
	private volatile long state = 0xCAFEBABE; // initial non-zero value

	private long nextLong() {
	  long a=state;
	  state = xorShift64(a);
	  return a;
	}

	private long xorShift64(long a) {
	  a ^= (a << 21);
	  a ^= (a >>> 35);
	  a ^= (a << 4);
	  return a;
	}

	public final int random(int n) {
	  if (n<0) throw new IllegalArgumentException();
	  long result=((nextLong()>>>32)*n)>>32;
	  return (int) result;
	}

}
