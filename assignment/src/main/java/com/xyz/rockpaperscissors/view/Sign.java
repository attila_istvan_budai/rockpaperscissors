package com.xyz.rockpaperscissors.view;

import com.xyz.rockpaperscissors.state.Shape;


public interface Sign {
	
	public String getSign();
	
	public Shape getShape();

}
