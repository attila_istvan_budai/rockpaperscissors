package com.xyz.rockpaperscissors;


import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.xyz.rockpaperscissors.ai.AIStrategy;
import com.xyz.rockpaperscissors.ai.RandomStrategy;
import com.xyz.rockpaperscissors.game.ConsoleGame;
import com.xyz.rockpaperscissors.game.ConsoleGameStopper;
import com.xyz.rockpaperscissors.game.GameStopper;
import com.xyz.rockpaperscissors.state.ShapeComparator;
import com.xyz.rockpaperscissors.utils.ConsoleInputHelper;
import com.xyz.rockpaperscissors.utils.PseudoRandomGenerator;
import com.xyz.rockpaperscissors.view.Fist;
import com.xyz.rockpaperscissors.view.FormingV;
import com.xyz.rockpaperscissors.view.OpenHand;
import com.xyz.rockpaperscissors.view.Sign;


@Configuration
class TestConfig {
	

	@Bean
	public List<Sign> allSigns(){
		List<Sign> signs = new ArrayList<Sign>();
		signs.add(new Fist());
		signs.add(new FormingV());
		signs.add(new OpenHand());
		
		return signs;	
	}
	
	
	@Bean
	public ConsoleGame game(){
		return new ConsoleGame();
	}
	
	@Bean
	public AIStrategy randomAI(){
		return new RandomStrategy();
	}
	
	@Bean
	public ShapeComparator comparator(){
		return new ShapeComparator();
	}
	
	@Bean
	public ConsoleInputHelper consoleInputHelper() throws IOException{
		ConsoleInputHelper consoleInputHelper = Mockito.mock(ConsoleInputHelper.class);
		when(consoleInputHelper.parseInteger())
		.thenReturn(1)
		.thenReturn(1);
		return consoleInputHelper;
	}
	
	@Bean
	public PseudoRandomGenerator randomGenerator(){
		return new PseudoRandomGenerator();
		
	}
	
	@Bean
	public GameStopper consoleGameStopper(){
		ConsoleGameStopper consoleGameStopper = Mockito.mock(ConsoleGameStopper.class);
		return consoleGameStopper;
	}
	

}

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
@EnableConfigurationProperties
public class FunctionalTest {
	
	@Autowired
	private ConsoleGame game;
	
	
	@Test
	public void evaulateRoundTest() throws IOException{	
		game.startGame();
		assertTrue(true);

	}
	

}
