package com.xyz.rockpaperscissors.game;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.xyz.rockpaperscissors.state.Shape;
import com.xyz.rockpaperscissors.state.ShapeComparator;
import com.xyz.rockpaperscissors.view.Fist;
import com.xyz.rockpaperscissors.view.OpenHand;

public class ConsoleGameTest {
	
	@InjectMocks
	private ConsoleGame game;
	
	  @Mock
	    private ShapeComparator shapeComparator;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	private void setupShapeComparator(int returnValue) {
		when(shapeComparator.compare(any(Shape.class), any(Shape.class))).thenReturn(returnValue);
	}
	
	@Test
	public void evaulateRound_drawtest(){
		setupShapeComparator(0);
		String result = game.evaulateRound(new Fist(), new OpenHand());
		
		assertEquals(Game.MSG_DRAW, result);
	}
	
	@Test
	public void evaulateRound_wintest(){
		setupShapeComparator(1);
		String result = game.evaulateRound(new Fist(), new OpenHand());
		
		assertEquals(Game.MSG_LOST, result);
		assertEquals(0, game.getPlayerScore());
		assertEquals(1, game.getBotScore());

	}
	
	@Test
	public void evaulateRound_losetest(){
		setupShapeComparator(-1);
		String result = game.evaulateRound(new Fist(), new OpenHand());
		
		assertEquals(Game.MSG_WON, result);
		assertEquals(1, game.getPlayerScore());
		assertEquals(0, game.getBotScore());

	}
	
}
