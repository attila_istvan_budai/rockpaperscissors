package com.xyz.rockpaperscissors.game;

public interface GameStopper {
	
	public void stopGame();

}
