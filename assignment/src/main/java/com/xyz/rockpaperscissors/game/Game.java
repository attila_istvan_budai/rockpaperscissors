package com.xyz.rockpaperscissors.game;

import org.springframework.beans.factory.annotation.Autowired;

import com.xyz.rockpaperscissors.state.ShapeComparator;
import com.xyz.rockpaperscissors.view.Sign;
public abstract class Game {
	
	@Autowired
	private ShapeComparator comparator;
	
	private int playerScore;
	private int botScore;
	private int currentRound;
	
	public static final String MSG_DRAW = "It is a draw";
	public static final String MSG_LOST = "You lost this round";
	public static final String MSG_WON = "You won this round";
	
	public void startGame() {
		beforeGame();
		int numberOfRounds = getNumberOfRounds();
		currentRound = 0;
		while (currentRound < numberOfRounds) {
			nextRound();
		}
		afterGame();
	}
	
	protected String evaulateRound(Sign mySign, Sign botSign) {
		int result = comparator.compare(mySign.getShape(), botSign.getShape());
		String resultMessage = "";
		if(result == 0){
			resultMessage = MSG_DRAW;
		} else if(result>0){
			resultMessage = MSG_LOST;
			botScore++;
		} else{
			resultMessage = MSG_WON;
			playerScore++;
		}
		return resultMessage;
	}
	
	protected void nextRound(){	
		try {
			currentRound++;
			playRound();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	protected abstract int getNumberOfRounds();
	
	protected abstract void playRound() throws Exception;
	
	public int getPlayerScore(){
		return playerScore;
	}
	
	public int getBotScore(){
		return botScore;
	}
	
	protected abstract void beforeGame();
	
	protected abstract void afterGame();

}
