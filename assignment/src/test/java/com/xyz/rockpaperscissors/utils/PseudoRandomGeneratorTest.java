package com.xyz.rockpaperscissors.utils;

import org.junit.Before;
import org.junit.Test;

import com.xyz.rockpaperscissors.utils.PseudoRandomGenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PseudoRandomGeneratorTest {
	
	PseudoRandomGenerator generator;
	
	@Before
	public void setup(){
		generator = new PseudoRandomGenerator();
	}
	
	@Test
	public void testRandomWithZero(){
		int res = generator.random(0);
		assertEquals(0, res);
	}
	
	@Test
	public void testRandomWithLarger(){
		int res = generator.random(10);
		assertTrue(res<10);
	}

}
