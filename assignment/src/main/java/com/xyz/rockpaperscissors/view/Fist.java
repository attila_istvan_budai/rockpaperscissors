package com.xyz.rockpaperscissors.view;

import com.xyz.rockpaperscissors.state.Shape;
import com.xyz.rockpaperscissors.utils.AsciiHandSigns;

public class Fist implements Sign {

	public String getSign() {
		return AsciiHandSigns.FIST;
	}

	public Shape getShape() {
		return Shape.ROCK;
	}

}
