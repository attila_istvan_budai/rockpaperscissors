package com.xyz.rockpaperscissors.ai;

import com.xyz.rockpaperscissors.view.Sign;



public interface AIStrategy {
	
	public Sign play();

}
