package com.xyz.rockpaperscissors.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleInputHelper {
	
	BufferedReader br = null;
	
	public void start(){
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public void end(){
	       if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	}
	
	public Integer parseInteger() throws IOException {
		String input = null;
		try {
			input = br.readLine();
			Integer num = Integer.parseInt(input);
			if (num < 1) {
				System.out.println("You cannot choose less than one");
				num = parseInteger();
			}
			return num;
		} catch (NumberFormatException e) {
			System.out.println("Your input " + input
					+ " is invalid. Please provide a valid number");
			return parseInteger();
		}
	}

}
