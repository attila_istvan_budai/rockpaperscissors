package com.xyz.rockpaperscissors.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AsciiHandSigns {
	
	//Rock, paper, scissors by Veronica Karlsson
	//https://www.asciiart.eu/people/body-parts/hand-gestures
	
	public static final String FIST = "    _______        \r\n" + 
			  "---'   ____)       \r\n" + 
			  "      (_____)      \r\n" + 
		      "      (_____)      \r\n" + 
		      "      (____)       \r\n" + 
		      "---.__(___)        \r\n";

	public static final String SCISSORS = "    _______        \r\n" + 
		          "---'   ____)____   \r\n" + 
		          "          ______)  \r\n" + 
		          "          _______) \r\n" + 
		          "         _______)  \r\n" + 
		          "---.__________)    \r\n";


	public static final String PAPER = "    _______        \r\n" + 
		       "---'   ____)____   \r\n" + 
		       "          ______)  \r\n" + 
		       "       __________) \r\n" + 
		       "      (____)       \r\n" + 
		       "---.__(___)        \r\n";
	
	public static String indexTemplate(String word){
		return  getCharactersNTimes(' ', word.length())+"   \r\n" + 
				getCharactersNTimes('_', word.length())+"___\r\n" + 
				getCharactersNTimes(' ', word.length())+"   \r\n" + 
				"|" + word + "|\r\n" + 
				getCharactersNTimes('_', word.length())+"___\r\n" + 
				getCharactersNTimes(' ', word.length())+"   \r\n";
	}
	
	private static String getCharactersNTimes(Character character, int times){
		StringBuilder sb = new StringBuilder();
		for(int i=0; i< times; i++){
			sb.append(character);
		}
		return sb.toString();
	}
	
	public static String reverseHandSign(String input){
		String[] rows = input.split("\\r?\\n");
		List<String> list = Arrays.stream(rows).map(x -> reverseHandSignRow(x)).collect(Collectors.toList());
		return String.join("\r\n", list);
		
	}

	
	public static String reverseHandSignRow(String input){

        StringBuilder input1 = new StringBuilder();
        input1.append(input);
        return input1.reverse().toString().replace("(", ")").replace(")", "(");
		
	}
	
	public static void printMultiSign(String...signs){
		List<String[]> asciiSigns = new ArrayList<String[]>();
		for (int s = 0; s < signs.length; s++) {
			asciiSigns.add(signs[s].split("\\r?\\n"));
		}
		
		for(int i=0; i<6; i++ ){
			for(String[] sign : asciiSigns){
				System.out.print(sign[i] + "   ");
				
			}
			System.out.println();
		}
	}
	

}
