package com.xyz.rockpaperscissors;


import org.junit.Before;
import org.junit.Test;

import com.xyz.rockpaperscissors.state.Shape;
import com.xyz.rockpaperscissors.state.ShapeComparator;

import static org.junit.Assert.assertEquals;

public class ShapeComparatorTest {
	
	ShapeComparator comp;
	
	@Before
	public void setup(){
		comp = new ShapeComparator();
	}
	
	@Test
	public void draw(){
		Shape shape1 = Shape.ROCK;
		Shape shape2 = Shape.ROCK;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(0, result);	
	}
	
	@Test
	public void rockwins(){
		Shape shape1 = Shape.ROCK;
		Shape shape2 = Shape.SCISSORS;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(1, result);	
	}
	
	@Test
	public void rockLose(){
		Shape shape1 = Shape.ROCK;
		Shape shape2 = Shape.PAPER;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(-1, result);	
	}
	
	@Test
	public void paperwins(){
		Shape shape1 = Shape.PAPER;
		Shape shape2 = Shape.ROCK;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(1, result);	
	}
	
	@Test
	public void paperLose(){
		Shape shape1 = Shape.PAPER;
		Shape shape2 = Shape.SCISSORS;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(-1, result);	
	}
	
	@Test
	public void scissorswins(){
		Shape shape1 = Shape.SCISSORS;
		Shape shape2 = Shape.PAPER;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(1, result);	
	}
	
	@Test
	public void scissorsLose(){
		Shape shape1 = Shape.SCISSORS;
		Shape shape2 = Shape.ROCK;
		int result = comp.compare(shape1, shape2);
		
		assertEquals(-1, result);	
	}

}
