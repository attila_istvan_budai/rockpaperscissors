package com.xyz.rockpaperscissors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.Banner;

import com.xyz.rockpaperscissors.game.ConsoleGame;



@SpringBootApplication
public class ConsoleApplication implements CommandLineRunner {

    @Autowired
    private ConsoleGame game;

    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(ConsoleApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
       
    }

    public void run(String... args) throws Exception {
    	 game.startGame();
       
    }
}