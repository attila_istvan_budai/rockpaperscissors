package com.xyz.rockpaperscissors.utils;

public interface RandomGenerator {
	
	public int random(int n);

}
