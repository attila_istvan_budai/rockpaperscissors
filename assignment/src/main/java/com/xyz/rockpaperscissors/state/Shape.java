package com.xyz.rockpaperscissors.state;

public enum Shape {
	ROCK, PAPER, SCISSORS
}
