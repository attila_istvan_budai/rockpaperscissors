package com.xyz.rockpaperscissors.state;

import java.util.Comparator;

public class ShapeComparator implements  Comparator<Shape>{

	public int compare(Shape shape1, Shape shape2) {
		if(shape1 == shape2){
			return 0;
		}
		if((shape1 == Shape.PAPER && shape2 == Shape.ROCK) ||
			(shape1 == Shape.ROCK && shape2 == Shape.SCISSORS) || 
			(shape1 == Shape.SCISSORS && shape2 == Shape.PAPER)){
			return 1;
		} else {
			return -1;
		}
		
	}

}
