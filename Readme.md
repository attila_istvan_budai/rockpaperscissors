#Rock Paper Scissors game

To build the project use the *mvn install* command

To run the application from the console use *java -jar rockpaperscissors-0.0.1-SNAPSHOT.jar* command in the target folder.
