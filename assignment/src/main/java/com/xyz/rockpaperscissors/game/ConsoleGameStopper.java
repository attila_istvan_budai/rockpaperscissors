package com.xyz.rockpaperscissors.game;

public class ConsoleGameStopper implements GameStopper {

	@Override
	public void stopGame() {
		System.exit(0);
	}

}
