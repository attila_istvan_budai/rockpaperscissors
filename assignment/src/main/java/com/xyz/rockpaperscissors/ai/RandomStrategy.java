package com.xyz.rockpaperscissors.ai;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.xyz.rockpaperscissors.utils.RandomGenerator;
import com.xyz.rockpaperscissors.view.Sign;

public class RandomStrategy implements AIStrategy {
	
	@Autowired
	@Qualifier("allSigns")
	private List<Sign> signs;
	
	@Autowired
	private RandomGenerator randomGenerator;

	public Sign play() {
		int rand = randomGenerator.random(signs.size());
		return signs.get(rand);
	}

}
