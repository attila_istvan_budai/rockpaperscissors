package com.xyz.rockpaperscissors.view;

import com.xyz.rockpaperscissors.state.Shape;
import com.xyz.rockpaperscissors.utils.AsciiHandSigns;

public class FormingV implements Sign {

	public String getSign() {
		return AsciiHandSigns.SCISSORS;
	}

	public Shape getShape() {
		return Shape.SCISSORS;
	}

}
