package com.xyz.rockpaperscissors;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.*;

import com.xyz.rockpaperscissors.ai.AIStrategy;
import com.xyz.rockpaperscissors.ai.RandomStrategy;
import com.xyz.rockpaperscissors.game.ConsoleGame;
import com.xyz.rockpaperscissors.game.ConsoleGameStopper;
import com.xyz.rockpaperscissors.game.GameStopper;
import com.xyz.rockpaperscissors.state.ShapeComparator;
import com.xyz.rockpaperscissors.utils.ConsoleInputHelper;
import com.xyz.rockpaperscissors.utils.PseudoRandomGenerator;
import com.xyz.rockpaperscissors.view.Fist;
import com.xyz.rockpaperscissors.view.FormingV;
import com.xyz.rockpaperscissors.view.OpenHand;
import com.xyz.rockpaperscissors.view.Sign;

@Configuration
public class Config {
	
	@Bean
	public List<Sign> allSigns(){
		List<Sign> signs = new ArrayList<Sign>();
		signs.add(new Fist());
		signs.add(new FormingV());
		signs.add(new OpenHand());
		
		return signs;	
	}
	
	@Bean
	public PseudoRandomGenerator randomGenerator(){
		return new PseudoRandomGenerator();
		
	}
	
	@Bean
	public ConsoleGame game(){
		return new ConsoleGame();
	}
	
	@Bean
	public AIStrategy randomAI(){
		return new RandomStrategy();
	}
	
	@Bean
	public ShapeComparator comparator(){
		return new ShapeComparator();
	}
	
	@Bean
	public ConsoleInputHelper consoleInputHelper(){
		return new ConsoleInputHelper();
	}
	
	@Bean
	public GameStopper consoleGameStopper(){
		return new ConsoleGameStopper();
	}
	

}
