package com.xyz.rockpaperscissors.view;

import com.xyz.rockpaperscissors.state.Shape;
import com.xyz.rockpaperscissors.utils.AsciiHandSigns;


public class OpenHand implements Sign {

	public String getSign() {
		return AsciiHandSigns.PAPER;
	}

	public Shape getShape() {
		return Shape.PAPER;
	}

}
