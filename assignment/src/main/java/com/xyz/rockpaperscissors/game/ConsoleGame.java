package com.xyz.rockpaperscissors.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.xyz.rockpaperscissors.ai.AIStrategy;
import com.xyz.rockpaperscissors.utils.AsciiHandSigns;
import com.xyz.rockpaperscissors.utils.ConsoleInputHelper;
import com.xyz.rockpaperscissors.view.Sign;

public class ConsoleGame extends Game {

	@Autowired
	@Qualifier("allSigns")
	private List<Sign> signs;

	@Autowired
	@Qualifier("randomAI")
	private AIStrategy bot;
	
	@Autowired
	@Qualifier("consoleGameStopper")
	private GameStopper stopper;
	
	@Autowired
	private ConsoleInputHelper consoleInputHelper;
	
	
	
	protected void beforeGame(){
		consoleInputHelper.start();
		
	}
	
	protected void afterGame(){
		consoleInputHelper.end();
		stopper.stopGame();
	}


	protected void playRound() throws IOException {
		System.out.println("=================================================");
		int selected = selectSign();
		Sign mySign = signs.get(selected - 1);
		Sign botSign = bot.play();
		System.out.println("------------------------------------------------");
		showPlay(mySign, botSign);
		evaulateRound(mySign, botSign);
		showCurrentResult();
	}

	private void showCurrentResult() {
		AsciiHandSigns.printMultiSign(AsciiHandSigns.indexTemplate("Your score "),
				AsciiHandSigns.indexTemplate("         " + Integer.toString(getPlayerScore()) + "         "),
				AsciiHandSigns.indexTemplate("         " + Integer.toString(getBotScore())+ "         "),
				AsciiHandSigns.indexTemplate(" Bot score"));

	}

	private void showPlay(Sign mySign, Sign botSign) {
		AsciiHandSigns.printMultiSign(AsciiHandSigns.indexTemplate("Your sign: "), mySign.getSign(),  AsciiHandSigns.reverseHandSign(botSign.getSign()), AsciiHandSigns.indexTemplate("Bot sign"));
				
	}

	private int selectSign() throws IOException {
		System.out.println("Which sign you would like to play?");

		List<String> list = new ArrayList<String>();
		for (int s = 0; s < signs.size(); s++) {
			list.add(AsciiHandSigns.indexTemplate(Integer.toString(s+1)));
			list.add( signs.get(s).getSign());
		}
		
		AsciiHandSigns.printMultiSign(list.toArray(new String[list.size()]));
		
		int selected = parseSignIndex( signs.size());
		return selected;
	}

	private Integer parseSignIndex( int length)
			throws IOException {
		Integer selected = consoleInputHelper.parseInteger();
		if (selected > length) {
			System.out.println("You cannot choose index which is larger than "
					+ length);
			selected = consoleInputHelper.parseInteger();
		}
		return selected;
	}

	@Override
	protected int getNumberOfRounds() {
		System.out.print("How many games would you like to play?");
		int numberOfRounds = 0;
		try {
			numberOfRounds = consoleInputHelper.parseInteger();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numberOfRounds;
	}


}
